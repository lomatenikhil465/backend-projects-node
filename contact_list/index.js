const express = require("express");
const path = require("path");
const port = 8000;

const db = require('./config/mangoose');
const Contact = require('./models/contact');

const app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.urlencoded());

app.use(express.static('assets'));

//middleware1
// app.use(function(req, res, next){
//   //console.log("Middleware 1 called");
//   req.myName = "Nikhil";
//   next();
// });

// //middleware2
// app.use(function(req, res, next){
//   //console.log("Middleware2 called");
//   console.log("My name is ", req.myName);
//   next();

// })

var contactList = [
  {
    name:"Nikhil",
    phone: "11111111111"
  },
  {
    name:"Vinay",
    phone: "1234567890"
  },
  {
    name:"Dinesh",
    phone: "6544567890"
  }
]

app.get("/", function (req, res) {
  //console.log(req.myName);
  Contact.find({}).catch((err) => {
    console.log("Error in fetching Contacts");
    return;
  }).then((contacts) => {
    return res.render("home", {
      title: "Contacts List",
      contacts_list: contacts
    });
  })
  
});

app.get("/practice", function (req, res) {
  return res.render("practice", {
    title: "Let us play with EJS",
  });
});

app.post("/create-contact", function (req, res)  {
  // contactList.push({
  //   name:req.body.name,
  //   phone: req.body.phone,
  // })

  //contactList.push(req.body);

  //Paricular path redirection
  //return res.redirect('/');

  Contact.create({
    name: req.body.name,
    phone: req.body.phone
  }).catch((err) => {
      console.log("Error creating a contact!!");
      return;
  }).then((newContact) => {
    console.log("*********", newContact);
    return res.redirect('back')
  })
  
  //If you don't know the previous url
  //return res.redirect('back');
  // return res.redirect('/practice');
})

// :phone --> this is params
// ?phone= --> this is query
//For deleting a contact
app.get('/delete-contact', function(req, res){
  //console.log(req.query);
  //get the id from query
  let id = req.query.id;
  
  // let contactIndex = contactList.findIndex(contact => contact.phone == phone);
  // if(contactIndex != -1){
  //   contactList.splice(contactIndex, 1);
  // }
  
  //find the contact in the database by using id
  Contact.findByIdAndDelete(id).catch((err)=>{
    console.log("Error in deleting contact in databse");
    return;
  }).then((contact) => {
    return res.redirect('back');
  })
  

});

app.listen(port, function (err) {
  if (err) {
    console.log("Error running the server : ", err);
  }

  console.log("My server is up and running");
});
