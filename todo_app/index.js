const express = require('express');
const app = express();
const port = 8000;
const expressLayout = require('express-ejs-layouts')

const db = require('./config/mongoose')

app.use(expressLayout)

//set up all routes
app.use('/', require('./routes'));

//set up the view engine
app.set('view engine', 'ejs');
app.set('views', './views');

//For Posting the form
app.use(express.urlencoded())

// for getting static
app.use(express.static('./assets'))

app.set('layout extractStyles', true);
app.set('layout extractScripts', true)

app.listen(port, function (err) {
    if (err) {
        console.log(`Error in running the server : ${err}`);
        return;
    }

    console.log(`Server is up and running on the port: ${port}`);
})
